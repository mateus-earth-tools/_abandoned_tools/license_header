#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : install.sh                                                    ##
##  Project   : license_header                                                ##
##  Date      : Feb 23, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2020                                                ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh

##----------------------------------------------------------------------------##
## Variables                                                                  ##
##----------------------------------------------------------------------------##
SCRIPT_DIR=$(pw_get_script_dir);

RES_DIR="${SCRIPT_DIR}/etc";
SRC_DIR="${SCRIPT_DIR}/src";

ETC_DIR="/etc/stdmatt/license_header";
BIN_DIR="/usr/local/bin";

PROGRAM_NAME="license-header";


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
pw_as_super_user mkdir -p "${ETC_DIR}";

## @todo(stdmatt): Copy everything under the folder...
pw_as_super_user cp "${RES_DIR}/stdmatt_header.txt" "${ETC_DIR}"
pw_as_super_user cp "${RES_DIR}/stdmatt_logo.txt"   "${ETC_DIR}"
pw_as_super_user cp "${SRC_DIR}/license_header.py"  "${BIN_DIR}/${PROGRAM_NAME}";
